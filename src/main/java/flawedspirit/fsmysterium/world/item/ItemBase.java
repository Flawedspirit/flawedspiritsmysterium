package flawedspirit.fsmysterium.world.item;

import flawedspirit.fsmysterium.FSMysterium;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class ItemBase extends Item {

	protected String unlocalizedName;
	
	public ItemBase(String unlocalizedName) {
		this.unlocalizedName = unlocalizedName;
		setUnlocalizedName(unlocalizedName);
		setRegistryName(unlocalizedName);
		
		setCreativeTab(FSMysterium.creativeTab);
	}
	
	@Override
	public boolean isEnchantable(ItemStack stack) {
		return false;
	}

	public void registerItemModel(Item item) {
		FSMysterium.proxy.registerItemRenderer(this, 0, unlocalizedName);
	}
}
