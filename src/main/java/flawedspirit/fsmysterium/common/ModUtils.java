package flawedspirit.fsmysterium.common;

import java.util.Locale;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ModUtils {

	public static final String MODID = "fsmysterium";
	public static final String MODNAME = "Flawedspirit's Mysterium";
	public static final String RESOURCEID = MODID.toLowerCase(Locale.CANADA);
	
	public static Logger getLogger(String type) {
		return LogManager.getLogger(MODID + "-" + type);
	}
	
	/**
	 * Returns a formatted resource string prefixed with the mod's resource locator.
	 */
	public static String resourceTag(String resource) {
		return String.format("%s:%s", RESOURCEID, resource);
	}
	
	/**
	 * Returns a formatted unlocalized string prefixed with the mod's identifier.
	 */
	public static String prefix(String name) {
		return String.format("%s.%s", RESOURCEID, name.toLowerCase(Locale.CANADA));
	}
}
