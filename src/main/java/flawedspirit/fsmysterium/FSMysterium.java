package flawedspirit.fsmysterium;

import java.util.Random;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import flawedspirit.fsmysterium.common.CommonProxy;
import flawedspirit.fsmysterium.common.ModUtils;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@Mod(
	modid = ModUtils.MODID,
	name = ModUtils.MODNAME,
	dependencies = "required-after:forge@[14.23.1.2555,)",
	acceptedMinecraftVersions = "[1.12,)"
)
public class FSMysterium {

	public static final String modID = ModUtils.MODID;
	public static final String modVersion = "${version}";
	
	
	public static final Logger log = LogManager.getLogger(modID);
	public static final Random random = new Random();
	
	@Mod.Instance(modID)
	public static FSMysterium instance;
	
	@SidedProxy(clientSide = "flawedspirit.fsmysterium.common.CommonProxy", serverSide = "flawedspirit.fsmysterium.common.CommonProxy")
	public static CommonProxy proxy;
	
	public static CreativeTabs creativeTab = new CreativeTabs(ModUtils.MODID) {
		@SideOnly(Side.CLIENT)
		@Override
		public ItemStack getTabIconItem() {
			return new ItemStack(Items.DIAMOND);
		}
	};
	
	@Mod.EventHandler
	public void preInit(FMLPreInitializationEvent event) {
		log.info("Steel thyself! " + ModUtils.MODNAME + " is upon you!");
	}
	
	@Mod.EventHandler
	public void postInit(FMLPostInitializationEvent event) {
		
	}
}
